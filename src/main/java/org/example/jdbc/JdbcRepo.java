package org.example.jdbc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.DirectFieldAccessor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.example.jdbc.Utils.also;

/**
 * @author sirenia
 */
@Slf4j
public class JdbcRepo<T> extends NamedParameterJdbcDaoSupport implements BaseRepo<T> {
    protected Class<T> entityClazz;
    public RowMapper<T> rowMapper;
    protected TableMeta tableMeta;
    protected Function<String, Long> idGenerator;
    protected volatile boolean initialized = false;
    protected Consumer<JdbcRepo<T>> initializer = repo -> {
        ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
        entityClazz = (Class<T>) type.getActualTypeArguments()[1];

        rowMapper = new BeanPropertyRowMapper(entityClazz);

        tableMeta = new TableMeta();

        tableMeta.setTableName(Utils.camelNameToUnderscoreName(entityClazz.getSimpleName()));
        tableMeta.setHistoryTableName(tableMeta.getTableName() + "_his");
        tableMeta.setPrimaryKeyColumnName("id");
        tableMeta.setPrimaryKeyFieldName("id");
        tableMeta.setVersionColumnName("opti_version");
        tableMeta.setVersionFieldName("optiVersion");

        ReflectionUtils.doWithFields(entityClazz, field -> {
            if (field.getAnnotation(Transient.class) != null) {
                return;
            }
            //it.setAccessible(true);
            String fieldName = field.getName();
            String columnName = Utils.camelNameToUnderscoreName(fieldName);
            tableMeta.addColumnMeta(also(new ColumnMeta(), columnMeta -> {
                columnMeta.setColumnName(columnName);
                columnMeta.setFieldName(fieldName);
                columnMeta.setField(field);
            }));
        }, f -> !Modifier.isStatic(f.getModifiers()) && !Modifier.isFinal(f.getModifiers()));
    };

    public void initIfNeed() {
        if (!initialized) {
            synchronized (this) {
                if (!initialized) {
                    initializer.accept(this);
                }
            }
        }
    }

    @Override
    public Collection<String> getFieldNames() {
        return tableMeta.getFieldNameMap().keySet();
    }

    @Override
    public List<T> queryAll() {
        initIfNeed();
        String sql = String.format("select * from %s", tableMeta.getTableName());
        log.debug("==> {}", sql);
        Map<String, Object> params = Utils.mapOf();
        log.debug("==> {}", params);
        List<T> rows = getNamedParameterJdbcTemplate().query(sql, params, rowMapper);
        log.debug("<== {}", rows);
        return rows;
    }

    @Override
    public List<T> queryByIds(List<Long> ids) {
        initIfNeed();
        String sql = String.format("select * from %s where %s in(:ids)",
                tableMeta.getTableName(), tableMeta.getPrimaryKeyColumnName());
        log.debug("==> {}", sql);
        Map<String, Object> params = Utils.mapOf("ids", ids);
        log.debug("==> {}", params);
        List<T> rows = getNamedParameterJdbcTemplate().query(sql, params, rowMapper);
        log.debug("<== {}", rows);
        return rows;
    }

    @Override
    public Long insertSelective(T t) {
        initIfNeed();
        DirectFieldAccessor acc = new DirectFieldAccessor(t);
        Object id = acc.getPropertyValue(tableMeta.getPrimaryKeyFieldName());
        if (id == null) {
            if (idGenerator != null) {
                acc.setPropertyValue(tableMeta.getPrimaryKeyFieldName(), idGenerator.apply(tableMeta.getTableName()));
            } else {
                return insertSelectiveAutoGenerateId(acc, t);
            }
        }
        insertSelectiveWithId(acc, t);
        return (Long) id;
    }

    private void insertSelectiveWithId(DirectFieldAccessor acc, T t) {
        initIfNeed();
        Tuple2<List<String>, List<String>> tuple2 = getColumnsAndValuesSelective(acc, ((columnMeta, o) -> true));
        String columns = String.join(",", tuple2._1);
        String values = String.join(",", tuple2._2);
        String sql = String.format("insert into %s(%s)values(%s)", tableMeta.getTableName(), columns, values);
        log.debug("==> {}", sql);
        log.debug("==> {}", t);
        int count = getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(t));
        log.debug("<== {}", count);
    }

    private Tuple2<List<String>, List<String>> getColumnsAndValuesSelective(DirectFieldAccessor acc, BiPredicate<ColumnMeta, Object> additionalPre) {
        initIfNeed();
        List<String> columns = new ArrayList<>();
        List<String> values = new ArrayList<>();
        tableMeta.getFieldNameMap().forEach((fieldName, columnMeta) -> {
            Object value = acc.getPropertyValue(fieldName);
            if (value != null && additionalPre.test(columnMeta, value)) {
                columns.add(columnMeta.getColumnName());
                values.add(":" + fieldName);
            }
        });
        return new Tuple2<>(columns, values);
    }

    private Long insertSelectiveAutoGenerateId(DirectFieldAccessor acc, T t) {
        initIfNeed();
        Tuple2<List<String>, List<String>> tuple2 = getColumnsAndValuesSelective(acc, ((columnMeta, o) -> true));
        String columns = String.join(",", tuple2._1);
        String values = String.join(",", tuple2._2);
        String sql = String.format("insert into %s(%s)values(%s)", tableMeta.getTableName(), columns, values);
        log.debug("==> {}", sql);
        log.debug("==> {}", t);
        GeneratedKeyHolder kh = new GeneratedKeyHolder();
        getNamedParameterJdbcTemplate().update(sql,
                new BeanPropertySqlParameterSource(t), kh, new String[]{tableMeta.getPrimaryKeyColumnName()});
        Long id = Utils.convert(kh.getKey(), Long.class);
        acc.setPropertyValue(tableMeta.getPrimaryKeyFieldName(), id);
        log.debug("<== {}", id);
        return id;
    }

    @Override
    public int updateSelective(T t) {
        initIfNeed();
        DirectFieldAccessor acc = new DirectFieldAccessor(t);
        Tuple2<List<String>, List<String>> tuple2 = getColumnsAndValuesSelective(acc, ((columnMeta, o) ->
                !columnMeta.getColumnName().equals(tableMeta.getVersionColumnName()) &&
                        !columnMeta.getColumnName().equals(tableMeta.getPrimaryKeyColumnName())
        ));
        List<String> columnsAndValues = Utils.zipWith(tuple2._1, tuple2._2, (column, value) -> column + "=" + value);
        String sets = String.join(",", columnsAndValues);
        String versionSet = String.format("%s=%s+1", tableMeta.getVersionColumnName(), tableMeta.getVersionColumnName());
        String sql = String.format("update %s set %s,%s where %s=:%s",
                tableMeta.getTableName(), sets, versionSet, tableMeta.getPrimaryKeyColumnName(), tableMeta.getPrimaryKeyFieldName());
        log.debug("==> {}", sql);
        log.debug("==> {}", t);
        int count = getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(t));
        log.debug("<== {}", count);
        return count;
    }

    @Override
    public int updateSelectiveWithOptiLock(T t) {
        initIfNeed();
        DirectFieldAccessor acc = new DirectFieldAccessor(t);
        Tuple2<List<String>, List<String>> tuple2 = getColumnsAndValuesSelective(acc, ((columnMeta, o) ->
                !columnMeta.getColumnName().equals(tableMeta.getVersionColumnName()) &&
                        !columnMeta.getColumnName().equals(tableMeta.getPrimaryKeyColumnName())
        ));
        List<String> columnsAndValues = Utils.zipWith(tuple2._1, tuple2._2, (column, value) -> column + "=" + value);
        String sets = String.join(",", columnsAndValues);
        String versionSet = String.format("%s=%s+1", tableMeta.getVersionColumnName(), tableMeta.getVersionColumnName());
        String sql = String.format("update %s set %s,%s where %s=:%s and %s=:%s",
                tableMeta.getTableName(), sets, versionSet, tableMeta.getPrimaryKeyColumnName(),
                tableMeta.getPrimaryKeyFieldName(), tableMeta.getVersionColumnName(), tableMeta.getVersionFieldName());
        log.debug("==> {}", sql);
        log.debug("==> {}", t);
        int count = getNamedParameterJdbcTemplate().update(sql, new BeanPropertySqlParameterSource(t));
        log.debug("<== {}", count);
        return count;
    }

    @Override
    public void batchInsert(Collection<String> includeFieldNames, List<T> list, int batchSize) {
        initIfNeed();
        if (batchSize < 1) {
            throw new IllegalArgumentException("batch size cannot smaller than 1!");
        }
        String columns = includeFieldNames.stream().map(Utils::camelNameToUnderscoreName).collect(Collectors.joining(","));
        String values = includeFieldNames.stream().map(it -> ":" + it).collect(Collectors.joining(","));
        String sql = String.format("insert into %s(%s) values (%s)", tableMeta.getTableName(), columns, values);
        withChunk(list, batchSize, chunk -> Arrays.stream(chunkInsert(chunk, sql)).sum());
    }

    private int[] chunkInsert(List<T> list, String sql) {
        BeanPropertySqlParameterSource[] sources = list.stream().map(
                BeanPropertySqlParameterSource::new
        ).toArray(BeanPropertySqlParameterSource[]::new);
        log.debug("==> {}", sql);
        log.debug("==> {}", list);
        int[] counts = getNamedParameterJdbcTemplate().batchUpdate(sql, sources);
        log.debug("<== {}", Arrays.asList(counts));
        return counts;
    }

    @Override
    public int batchUpdate(Collection<String> includeFieldNames, List<T> list, int batchSize) {
        initIfNeed();
        if (batchSize < 1) {
            throw new IllegalArgumentException("batch size cannot smaller than 1!");
        }
        String sets = includeFieldNames.stream().filter(it ->
                        !it.equals(tableMeta.getVersionFieldName()) && !it.equals(tableMeta.getPrimaryKeyFieldName())
                ).map(it -> Utils.camelNameToUnderscoreName(it) + "=:" + it)
                .collect(Collectors.joining(","));
        String versionSet = String.format("%s=%s+1", tableMeta.getVersionColumnName(), tableMeta.getVersionColumnName());
        String sql = String.format("update %s set %s,%s where %s=:%s",
                tableMeta.getTableName(), sets, versionSet, tableMeta.getPrimaryKeyColumnName(), tableMeta.getPrimaryKeyFieldName());
        return withChunk(list, batchSize, chunk -> Arrays.stream(chunkUpdate(sql, chunk)).sum());
    }

    private int[] chunkUpdate(String sql, List<T> chunk) {
        SqlParameterSource[] sources = chunk.stream().map(BeanPropertySqlParameterSource::new)
                .toArray(SqlParameterSource[]::new);
        log.debug("==> {}", sql);
        log.debug("==> {}", chunk);
        int[] counts = getNamedParameterJdbcTemplate().batchUpdate(sql, sources);
        log.debug("<== {}", Arrays.asList(counts));
        return counts;
    }

    @Override
    public int batchUpdateWithOptiVersion(Collection<String> includeFieldNames, List<T> list, int batchSize) {
        initIfNeed();
        if (batchSize < 1) {
            throw new IllegalArgumentException("batch size cannot smaller than 1!");
        }
        String sets = includeFieldNames.stream().filter(it ->
                        !it.equals(tableMeta.getVersionFieldName()) && !it.equals(tableMeta.getPrimaryKeyFieldName())
                ).map(it -> Utils.camelNameToUnderscoreName(it) + "=:" + it)
                .collect(Collectors.joining(","));
        String versionSet = String.format("%s=%s+1", tableMeta.getVersionColumnName(), tableMeta.getVersionColumnName());
        String sql = String.format("update %s set %s,%s where %s=:%s and %s=:%s",
                tableMeta.getTableName(), sets, versionSet, tableMeta.getPrimaryKeyColumnName(), tableMeta.getPrimaryKeyFieldName(),
                tableMeta.getVersionColumnName(), tableMeta.getVersionFieldName());
        return withChunk(list, batchSize, chunk -> Arrays.stream(chunkUpdate(sql, chunk)).sum());
    }

    @Override
    public Long insertOrUpdateSelective(T t) {
        initIfNeed();
        DirectFieldAccessor acc = new DirectFieldAccessor(t);
        Long id = (Long) acc.getPropertyValue(tableMeta.getPrimaryKeyFieldName());
        if (id != null) {
            long cnt = countById(id);
            if (cnt > 0) {
                long uptCnt = updateSelective(t);
                if (uptCnt != 1) {
                    throw new RuntimeException("expect updated 1 record, but updated " + uptCnt);
                }
                return id;
            }
        }
        return insertSelective(t);
    }

    private long countById(Long id) {
        String sql = String.format("select count(*) from %s where %s=:id", tableMeta.getTableName(), tableMeta.getPrimaryKeyColumnName());
        log.debug("==> {}", sql);
        log.debug("==> {}", id);
        long count = getNamedParameterJdbcTemplate().queryForObject(sql, Utils.mapOf("id", id), Long.class);
        log.debug("<== {}", count);
        return count;
    }

    @Override
    public Long insertOrUpdateSelectiveWithOptLock(T t) {
        initIfNeed();
        DirectFieldAccessor acc = new DirectFieldAccessor(t);
        Long id = (Long) acc.getPropertyValue(tableMeta.getPrimaryKeyFieldName());
        if (id != null) {
            long cnt = countById(id);
            if (cnt > 0) {
                long uptCnt = updateSelectiveWithOptiLock(t);
                if (uptCnt != 1) {
                    throw new RuntimeException("expect updated 1 record, but updated " + uptCnt);
                }
                return id;
            }
        }
        return insertSelective(t);
    }

    @Override
    public <R> PageRes<R> queryPagination(String sql, Object params, PageParam pageParam, Class<R> clazz) {
        initIfNeed();
        sql = sql.trim();
        if (sql.endsWith(";")) {
            sql = sql.substring(0, sql.length() - 1);
        }
        SqlParameterSource parameterSource;
        if (params instanceof Map) {
            parameterSource = new MapSqlParameterSource((Map<String, ?>) params);
        } else {
            parameterSource = new BeanPropertySqlParameterSource(params);
        }
        PageRes<R> pageRes = new PageRes<>();
        String countSql = String.format("select count(*) from (%s) _txpqmsf", sql);
        log.debug("==> {}", countSql);
        log.debug("==> {}", params);
        Integer total = getNamedParameterJdbcTemplate().queryForObject(countSql, parameterSource, Integer.class);
        log.debug("<== {}", total);
        pageRes.setTotal(total);
        if (total > pageParam.offset()) {
            String rowsSql = String.format("%s limit %s offset %s", sql, pageParam.getSize(), pageParam.offset());
            log.debug("==> {}", rowsSql);
            log.debug("==> {}", params);
            List<R> rows = getNamedParameterJdbcTemplate().query(rowsSql, parameterSource, new BeanPropertyRowMapper<>(clazz));
            log.debug("<== {}", rows);
            pageRes.setRows(rows);
        } else {
            pageRes.setRows(new ArrayList<>());
        }
        return pageRes;
    }

    @Override
    public int deleteByIds(List<Long> ids, int batchSize) {
        initIfNeed();
        String bakSql = String.format("insert into %s(%s) select %s from %s where %s in(:ids)",
                tableMeta.getHistoryTableName(), tableMeta.getColumnNameMap().keySet(),
                tableMeta.getColumnNameMap().keySet(), tableMeta.getTableName(), tableMeta.getPrimaryKeyColumnName());
        String sql = String.format("delete from %s where %s in(:ids)",
                tableMeta.getTableName(), tableMeta.getPrimaryKeyColumnName());
        ids.stream().map(String::valueOf).collect(Collectors.joining(","));
        return withChunk(ids, batchSize, chunk -> {
            Map<String, Object> params = Utils.mapOf("ids", ids);
            log.debug("==> {}", bakSql);
            log.debug("==> {}", ids);
            int bakCount = getNamedParameterJdbcTemplate().update(bakSql, params);
            log.debug("<== {}", bakCount);
            log.debug("==> {}", sql);
            log.debug("==> {}", ids);
            int count = getNamedParameterJdbcTemplate().update(sql, params);
            log.debug("<== {}", count);
            return count;
        });
    }

    @Override
    public int deleteByIdWithOptiVersion(Long id, Integer version) {
        initIfNeed();
        String bakSql = String.format("insert into %s(%s) select %s from %s where %s = :id and %s = :version",
                tableMeta.getHistoryTableName(), tableMeta.getColumnNameMap().keySet(),
                tableMeta.getColumnNameMap().keySet(), tableMeta.getTableName(), tableMeta.getPrimaryKeyColumnName(),
                tableMeta.getVersionColumnName());
        Map<String, Object> params = Utils.mapOf("id", id, "version", version);
        log.debug("==> {}", bakSql);
        log.debug("==> {}", id);
        int bakCount = getNamedParameterJdbcTemplate().update(bakSql, params);
        log.debug("<== {}", bakCount);
        if (bakCount == 0) {
            return 0;
        }
        String sql = String.format("delete from %s where %s = :id and %s = :version",
                tableMeta.getTableName(), tableMeta.getPrimaryKeyColumnName(), tableMeta.getVersionColumnName());
        log.debug("==> {}", sql);
        log.debug("==> {}", id);
        int count = getNamedParameterJdbcTemplate().update(sql, params);
        log.debug("<== {}", count);
        return count;
    }

    protected <P> int withChunk(List<P> list, int batchSize, Function<List<P>, Integer> fn) {
        int from = 0;
        int to = Math.min(list.size(), batchSize);
        int count = 0;
        while (from < to) {
            List<P> sublist = list.subList(from, to);
            count += fn.apply(sublist);
            from = to;
            to = Math.min(list.size(), to + batchSize);
        }
        return count;
    }
}
