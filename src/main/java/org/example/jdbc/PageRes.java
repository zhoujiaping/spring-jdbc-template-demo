package org.example.jdbc;

import lombok.Data;

import java.util.List;

@Data
public class PageRes<T> {
    Integer total;
    List<T> rows;
}
