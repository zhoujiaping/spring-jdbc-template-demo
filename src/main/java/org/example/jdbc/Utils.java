package org.example.jdbc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.core.convert.support.DefaultConversionService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public abstract class Utils {
    private Utils(){}
    public static final DefaultConversionService converterService = new DefaultConversionService();
    public static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    public static <T> T also(T t, Consumer<T> fn){
        fn.accept(t);
        return t;
    }
    public static <T,R> R let(T t, Function<T,R> fn){
        return fn.apply(t);
    }
    public static String camelNameToUnderscoreName(String uncapitalizedName) {
        return uncapitalizedName.replaceAll("(?<Uper>[A-Z])", "_${Uper}").toLowerCase();
    }
    public static Map<String,Object> mapOf(Object... items){
        Map<String,Object> map = new HashMap<>();
        if(items == null){
            return map;
        }
        if(items.length%2 != 0){
            throw new IllegalArgumentException("parameter must be even");
        }
        for(int i=0;i<items.length;i=i+2){
            map.put(items[i].toString(),items[i+1]);
        }
        return map;
    }
    public static <T> T convert(Object source,Class<T> targetClass){
        return converterService.convert(source,targetClass);
    }
    public static <T,P1,P2> List<T> zipWith(List<P1> list1, List<P2> list2, BiFunction<P1,P2,T> fn){
        int size = Math.max(list1.size(), list2.size());
        List<T> res = new ArrayList<>();
        for(int i=0;i<size;i++){
            res.add(fn.apply(list1.get(i),list2.get(i)));
        }
        return res;
    }
    public static String toJson(Object obj){
        return gson.toJson(obj);
    }
    public static <T> T fromJson(String json,Class<T> clazz){
        return gson.fromJson(json,clazz);
    }
}
