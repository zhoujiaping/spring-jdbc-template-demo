package org.example.jdbc;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
public class TableMeta {
    private String tableName;
    private String historyTableName;
    private String primaryKeyColumnName;
    private String primaryKeyFieldName;
    private String versionColumnName;
    private String versionFieldName;
    private Map<String,ColumnMeta> columnNameMap = new LinkedHashMap<>();
    private Map<String,ColumnMeta> fieldNameMap = new LinkedHashMap<>();

    public TableMeta addColumnMeta(ColumnMeta columnMeta){
        columnNameMap.put(columnMeta.getColumnName(),columnMeta);
        fieldNameMap.put(columnMeta.getFieldName(),columnMeta);
        return this;
    }
}
