package org.example.jdbc;

import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.List;

interface BaseRepo<T> {
    Collection<String> getFieldNames();

    List<T> queryAll();

    default T queryById(Long id){
        List<T> list = queryByIds(Lists.newArrayList(id));
        if(list.isEmpty()){
            return null;
        }
        return list.get(0);
    }

    List<T> queryByIds(List<Long> id);

    Long insertSelective(T t);

    int updateSelective(T t);

    int updateSelectiveWithOptiLock(T t);

    void batchInsert(Collection<String> includeFieldNames, List<T> list, int batchSize);

    default void batchInsert(Collection<String> includeFieldNames, List<T> list) {
        batchInsert(includeFieldNames, list, 100);
    }

    default void batchInsert(List<T> list) {
        batchInsert(Lists.newArrayList(getFieldNames()), list);
    }

    int batchUpdate(Collection<String> includeFieldNames, List<T> list, int batchSize);

    int batchUpdateWithOptiVersion(Collection<String> includeFieldNames, List<T> list, int batchSize);

    Long insertOrUpdateSelective(T t);

    Long insertOrUpdateSelectiveWithOptLock(T t);

    <R> PageRes<R> queryPagination(String sql, Object params, PageParam pageParam, Class<R> resultClass);

    default int deleteById(Long id) {
        return deleteByIds(Lists.newArrayList(id));
    }

    int deleteByIds(List<Long> ids, int batchSize);

    int deleteByIdWithOptiVersion(Long id, Integer version);

    default int deleteByIds(List<Long> ids) {
        return deleteByIds(ids, 100);
    }
}