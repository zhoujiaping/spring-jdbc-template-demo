package org.example.jdbc;

import lombok.Data;

import java.lang.reflect.Field;

@Data
public class ColumnMeta {
    private String columnName;
    private String fieldName;
    private Field field;
}
