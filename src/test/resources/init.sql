create table t_order(
    id bigint,
    tenant_id bigint,
    name varchar(64),
    remarks varchar(256),
    creator_id bigint,
    create_time datetime default now(),
    last_updater_id bigint,
    last_update_time datetime default now(),
    version int
);
create table t_order_his(
    id bigint,
    tenant_id bigint,
    name varchar(64),
    remarks varchar(256),
    creator_id bigint,
    create_time datetime default now(),
    last_updater_id bigint,
    last_update_time datetime default now(),
    version int
);


