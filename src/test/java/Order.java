import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Order {
    Long id;
    String name;
    String remarks;
    Integer version;
    Long lastUpdaterId;
    Date lastUpdateTime;
    Long creatorId;
    Date createTime;
    Long tenantId;
}
