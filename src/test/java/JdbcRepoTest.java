import com.alibaba.druid.pool.DruidDataSource;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.example.jdbc.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static org.example.jdbc.Utils.also;

/**
 * TODO 代码重构后，单元测试还没来得及修改及验证
 */
public class JdbcRepoTest {
    Gson gson = new Gson();
    AtomicLong generator = new AtomicLong();
    JdbcRepo<Order> orderRepo = new JdbcRepo<Order>(){
        {
            this.initializer = (repo)->{
                ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
                entityClazz = (Class<Order>) type.getActualTypeArguments()[1];

                rowMapper = new BeanPropertyRowMapper(entityClazz);

                tableMeta = new TableMeta();

                tableMeta.setTableName("t_"+Utils.camelNameToUnderscoreName(entityClazz.getSimpleName()));
                tableMeta.setHistoryTableName(tableMeta.getTableName() + "_his");
                tableMeta.setPrimaryKeyColumnName("id");
                tableMeta.setPrimaryKeyFieldName("id");
                tableMeta.setVersionColumnName("version");
                tableMeta.setVersionFieldName("version");

                ReflectionUtils.doWithFields(entityClazz, field -> {
                    //it.setAccessible(true);
                    String fieldName = field.getName();
                    String columnName = Utils.camelNameToUnderscoreName(fieldName);
                    tableMeta.addColumnMeta(also(new ColumnMeta(), columnMeta -> {
                        columnMeta.setColumnName(columnName);
                        columnMeta.setFieldName(fieldName);
                        columnMeta.setField(field);
                    }));
                }, f -> !Modifier.isStatic(f.getModifiers()) && !Modifier.isFinal(f.getModifiers()));
            };
            this.idGenerator = tableName->generator.getAndIncrement();
        }
    };
    
    {
        //设置数据源
        DruidDataSource ds = new DruidDataSource();
        Properties props = new Properties();
        props.putAll(Utils.mapOf(
                "druid.url","jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8",
                "druid.username","root",
                "druid.password","123456",
                "druid.driverClassName","com.mysql.cj.jdbc.Driver"
        ));
        ds.configFromPropety(props);
        orderRepo.setDataSource(ds);
    }
    @BeforeEach
    public void beforeEach(){
        generator.set(100L);
        orderRepo.getNamedParameterJdbcTemplate().update("truncate table t_order",Utils.mapOf());
        orderRepo.getNamedParameterJdbcTemplate().update("truncate table t_order_bak",Utils.mapOf());
    }
    @Test
    public void testInsertSelective(){
        Order order = Order.builder().name("京东").remarks("重要的订单").build();
        Long id = orderRepo.insertSelective(order);
        System.out.println(gson.toJson(order));
        Assertions.assertEquals(100L,order.getId());
        Assertions.assertEquals("重要的订单",order.getRemarks());
        Assertions.assertEquals(8L,order.getTenantId());
        Assertions.assertEquals(88L,order.getCreatorId());
        Assertions.assertNotNull(order.getCreateTime());
        Assertions.assertEquals("京东",order.getName());
        Assertions.assertEquals(1,order.getVersion());
        Assertions.assertNotNull(order.getLastUpdateTime());
        Assertions.assertEquals(88L,order.getLastUpdaterId());
    }
    @Test
    public void testDelete(){
        orderRepo.insertSelective(Order.builder().name("京东").build());
        int cnt = orderRepo.deleteById(100L);
        System.out.println(cnt);
        Assertions.assertEquals(1,cnt);
    }
    @Test
    public void testUpdateSelective(){
        orderRepo.insertSelective(Order.builder().name("京东").build());

        Order order = Order.builder().id(100L).version(5).name("当当")
                .creatorId(99L).build();
        int cnt = orderRepo.updateSelective(order);
        System.out.println(cnt);
        Assertions.assertEquals(1,cnt);
        Assertions.assertEquals(100L,order.getId());
        Assertions.assertNull(order.getRemarks());
        Assertions.assertEquals(8L,order.getTenantId());
        Assertions.assertEquals(99L,order.getCreatorId());
        Assertions.assertNull(order.getCreateTime());
        Assertions.assertEquals("当当",order.getName());
        Assertions.assertEquals(null, order.getVersion());
        Assertions.assertNotNull(order.getLastUpdateTime());
        Assertions.assertEquals(89L,order.getLastUpdaterId());
    }
    @Test
    public void testUpdateSelectiveWithOptiLock(){
        orderRepo.insertSelective(Order.builder().name("京东").build());
        Order order = Order.builder().id(100L).version(1).name("当当")
                .creatorId(null).build();
        int cnt = orderRepo.updateSelectiveWithOptiLock(order);
        System.out.println(cnt);
        Assertions.assertEquals(1,cnt);
        Assertions.assertEquals(100L,order.getId());
        Assertions.assertNull(order.getRemarks());
        Assertions.assertEquals(8L,order.getTenantId());
        Assertions.assertNull(order.getCreatorId());
        Assertions.assertNull(order.getCreateTime());
        Assertions.assertEquals("当当",order.getName());
        Assertions.assertEquals(2, order.getVersion());
        Assertions.assertNotNull(order.getLastUpdateTime());
        Assertions.assertEquals(89L,order.getLastUpdaterId());
    }
    @Test
    public void testUpdateSelectiveWithOptiLockFail(){
        orderRepo.insertSelective(Order.builder().name("京东").build());
        Order order = Order.builder().id(100L).version(2).name("当当")
                .creatorId(99L).build();
        int cnt = orderRepo.updateSelectiveWithOptiLock(order);
        System.out.println(cnt);
        Assertions.assertEquals(0,cnt);
        Assertions.assertEquals(100L,order.getId());
        Assertions.assertNull(order.getRemarks());
        Assertions.assertEquals(8L,order.getTenantId());
        Assertions.assertEquals(99L,order.getCreatorId());//创建者不支持更改，所以即使传了该参数，数据库里面值也不会改
        Assertions.assertNull(order.getCreateTime());
        Assertions.assertEquals("当当",order.getName());
        Assertions.assertEquals(3, order.getVersion());
        Assertions.assertNotNull(order.getLastUpdateTime());
        Assertions.assertEquals(89L,order.getLastUpdaterId());
    }
    @Test
    public void testCustomQuery(){
        List<Order> orders = Lists.newArrayList(
                Order.builder().name("淘宝").build(),
                Order.builder().name("当当").build(),
                Order.builder().name("拼夕夕").build(),
                Order.builder().name("京东-北京").build(),
                Order.builder().name("京东-上海").build()
        );
        orderRepo.batchInsert(orderRepo.getFieldNames(),orders,2);
        String sql = "select * from t_order where name like :name";
        OrderDTO param = OrderDTO.builder().name("京%").build();
        List<Order> list = orderRepo.getNamedParameterJdbcTemplate()
                .query(sql,new BeanPropertySqlParameterSource(param), orderRepo.rowMapper);
        System.out.println(gson.toJson(list));
        Assertions.assertEquals(2,list.size());

        Assertions.assertEquals(103L,list.get(0).getId());
        Assertions.assertNull(list.get(0).getRemarks());
        Assertions.assertEquals(8L,list.get(0).getTenantId());
        Assertions.assertEquals(88L,list.get(0).getCreatorId());
        Assertions.assertNotNull(list.get(0).getCreateTime());
        Assertions.assertEquals("京东-北京",list.get(0).getName());
        Assertions.assertEquals(1, list.get(0).getVersion());
        Assertions.assertNotNull(list.get(0).getLastUpdateTime());
        Assertions.assertEquals(88L,list.get(0).getLastUpdaterId());

        Assertions.assertEquals("京东-上海",list.get(1).getName());
        Assertions.assertEquals(104L,list.get(1).getId());
        Assertions.assertEquals(88L,list.get(1).getCreatorId());
        Assertions.assertNotNull(list.get(1).getLastUpdateTime());
        Assertions.assertEquals(1,list.get(1).getVersion());
    }

    @Test
    public void testQueryByPage(){
        List<Order> orders = Lists.newArrayList(
                Order.builder().name("淘宝").build(),
                Order.builder().name("京东-南京").build(),
                Order.builder().name("京东-广州").build(),
                Order.builder().name("京东-长沙").build(),
                Order.builder().name("京东-北京").build(),
                Order.builder().name("京东-上海").build()
        );
        orderRepo.batchInsert(orderRepo.getFieldNames(), orders,2);
        String sql = "select * from t_order where name like :name";
        OrderDTO param = OrderDTO.builder().name("京东%").build();
        PageParam pageParam = PageParam.builder().page(1).size(3).build();
        PageRes<OrderDTO> pageRes = orderRepo.queryPagination(sql,
                new BeanPropertySqlParameterSource(param),pageParam,OrderDTO.class);
        System.out.println(gson.toJson(pageRes));
        Assertions.assertEquals(5,pageRes.getTotal());
        Assertions.assertEquals(3,pageRes.getRows().size());
    }
    @Test
    public void testQueryByIdList(){
        List<Order> orders = Lists.newArrayList(
                Order.builder().name("淘宝").build(),
                Order.builder().name("京东-南京").build(),
                Order.builder().name("京东-广州").build(),
                Order.builder().name("京东-长沙").build(),
                Order.builder().name("京东-北京").build(),
                Order.builder().name("京东-上海").build()
        );
        orderRepo.batchInsert(orderRepo.getFieldNames(), orders,2);
        List<Long> ids = Lists.newArrayList(100L,102L,104L,106L,108L);
        List<Order> res = orderRepo.queryByIds(ids);
        System.out.println(gson.toJson(res));
        Assertions.assertEquals(3,res.size());
        Assertions.assertEquals("淘宝",res.get(0).getName());
        Assertions.assertEquals("京东-广州",res.get(1).getName());
        Assertions.assertEquals("京东-北京",res.get(2).getName());
    }
    @Test
    public void testBatchInsert(){
        List<Order> orders = Lists.newArrayList(
                Order.builder().name("淘宝").build(),
                Order.builder().name("当当").build(),
                Order.builder().name("拼夕夕").build()
        );
        orderRepo.batchInsert(orderRepo.getFieldNames(), orders,2);
        System.out.println(gson.toJson(orders));
        Assertions.assertEquals(3,orders.size());
        Assertions.assertEquals("淘宝",orders.get(0).getName());
        Assertions.assertEquals("当当",orders.get(1).getName());
        Assertions.assertEquals("拼夕夕",orders.get(2).getName());
    }
    @Test
    public void testBatchUpdate(){
        List<Order> orders = Lists.newArrayList(
                Order.builder().name("淘宝").build(),
                Order.builder().name("当当").build(),
                Order.builder().name("拼夕夕").build()
        );
        orderRepo.batchInsert(orderRepo.getFieldNames(), orders,2);
        Date date = new Date();
        List<Order> orderList = Lists.newArrayList(
                Order.builder().id(100L).name("淘宝").creatorId(-2L).lastUpdateTime(date).version(5).build(),
                Order.builder().id(101L).name("当当当").creatorId(-5L).lastUpdateTime(date).version(1).build(),
                Order.builder().id(102L).name("拼夕夕").creatorId(-4L).lastUpdateTime(null).version(100).build()
        );
        int cnt = orderRepo.batchUpdate(orderRepo.getFieldNames(), orderList,2);
        Order dangdang = orderRepo.queryById(101L);
        System.out.println(cnt);
        Assertions.assertEquals(3,cnt);
        Assertions.assertEquals(101L, orderList.get(1).getId());
        Assertions.assertEquals("当当当", orderList.get(1).getName());
        Assertions.assertEquals(-5L, orderList.get(1).getCreatorId());//但是数据库里面的值不会改
        Assertions.assertEquals(88L,dangdang.getCreatorId());//但是数据库里面的值不会改
        orderList.forEach(t->{
            Assertions.assertNotNull(orderList.get(1).getLastUpdateTime());
            Assertions.assertEquals(2, orderList.get(1).getVersion());
        });
        Assertions.assertNotEquals(date, orderList.get(1).getLastUpdateTime());
    }
    @Test
    public void testBatchUpdateWithOptiVersion(){
        List<Order> orders = Lists.newArrayList(
                Order.builder().name("淘宝").build(),
                Order.builder().name("当当").build(),
                Order.builder().name("拼夕夕").build()
        );
        orderRepo.batchInsert(orderRepo.getFieldNames(), orders,2);
        Date date = new Date();
        List<Order> orderList = Lists.newArrayList(
                Order.builder().id(100L).name("淘宝").creatorId(-2L).lastUpdateTime(date).version(5).build(),
                Order.builder().id(101L).name("当当当").creatorId(-5L).lastUpdateTime(date).version(1).build(),
                Order.builder().id(102L).name("拼夕夕").creatorId(-4L).lastUpdateTime(null).version(100).build()
        );
        int cnt = orderRepo.batchUpdateWithOptiVersion(orderRepo.getFieldNames(), orderList,2);
        Order dangdang = orderRepo.queryById(101L);
        System.out.println(cnt);
        Assertions.assertEquals(1,cnt);
        Assertions.assertEquals(101L, orderList.get(1).getId());
        Assertions.assertEquals("当当当", orderList.get(1).getName());
        Assertions.assertEquals(-5L, orderList.get(1).getCreatorId());//但是数据库里面的值不会改
        Assertions.assertEquals(88L,dangdang.getCreatorId());//但是数据库里面的值不会改
        orderList.forEach(t->{
            Assertions.assertNotNull(orderList.get(1).getLastUpdateTime());
            Assertions.assertEquals(2, orderList.get(1).getVersion());
        });
        Assertions.assertNotEquals(date, orderList.get(1).getLastUpdateTime());
    }
    @Test
    public void testQueryAll() {
        List<Order> orders = Lists.newArrayList(
                Order.builder().name("淘宝").build(),
                Order.builder().name("当当").build(),
                Order.builder().name("拼夕夕").build()
        );
        orderRepo.batchInsert(orderRepo.getFieldNames(), orders, 2);
        List<Order> rows = orderRepo.queryAll();
        Assertions.assertEquals(3,rows.size());
        Assertions.assertEquals(100L,rows.get(0).getId());
        Assertions.assertEquals(88L,rows.get(0).getCreatorId());
        Assertions.assertEquals(1,rows.get(0).getVersion());
        Assertions.assertEquals("淘宝",rows.get(0).getName());
        Assertions.assertNotNull(rows.get(0).getLastUpdateTime());
    }
    @Test
    public void testQueryById() {
        List<Order> orders = Lists.newArrayList(
                Order.builder().name("淘宝").build(),
                Order.builder().name("当当").build(),
                Order.builder().name("拼夕夕").build()
        );
        orderRepo.batchInsert(orderRepo.getFieldNames(), orders, 2);
        Order order = orderRepo.queryById(101L);
        Assertions.assertEquals(101L, order.getId());
        Assertions.assertEquals(88L, order.getCreatorId());
        Assertions.assertEquals(1, order.getVersion());
        Assertions.assertEquals("当当", order.getName());
        Assertions.assertNotNull(order.getLastUpdateTime());
    }
    @Test
    public void testInsertOrUpdateSelective(){
        Order order = Order.builder().name("京东").build();
        Long id = orderRepo.insertOrUpdateSelective(order);
        System.out.println(gson.toJson(order));
        Assertions.assertEquals(100L,order.getId());
        Assertions.assertEquals(88L,order.getCreatorId());
        Assertions.assertEquals("京东",order.getName());
        Assertions.assertEquals(1,order.getVersion());
        Assertions.assertNotNull(order.getLastUpdateTime());
    }
    @Test
    public void testInsertOrUpdateSelectiveWithOptiVersion(){
        Order order = Order.builder().id(100L).name("京东").build();
        Long id = orderRepo.insertOrUpdateSelectiveWithOptLock(order);
        System.out.println(gson.toJson(order));
        Assertions.assertEquals(100L,order.getId());
        Assertions.assertEquals(88L,order.getCreatorId());
        Assertions.assertEquals("京东",order.getName());
        Assertions.assertEquals(1,order.getVersion());
        Assertions.assertNotNull(order.getLastUpdateTime());

        Order order2 = Order.builder().id(100L).name("京东2").version(1).build();
        Long id2 = orderRepo.insertOrUpdateSelectiveWithOptLock(order2);
        Assertions.assertEquals("京东2",order2.getName());
    }
    @Test
    public void testBatchDelete(){
        List<Order> orders = Lists.newArrayList(
                Order.builder().name("淘宝").build(),
                Order.builder().name("京东-南京").build(),
                Order.builder().name("京东-广州").build(),
                Order.builder().name("京东-长沙").build(),
                Order.builder().name("京东-北京").build(),
                Order.builder().name("京东-上海").build()
        );
        orderRepo.batchInsert(orderRepo.getFieldNames(), orders,2);
        List<Long> ids = Lists.newArrayList(100L,102L,104L,106L,108L);
        int cnt = orderRepo.deleteByIds(ids,2);
        Assertions.assertEquals(3,cnt);
    }
}
